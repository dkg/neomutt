Fixes warnings from groff and mandoc.

https://bugs.debian.org/964416

--- a/docs/neomutt.man
+++ b/docs/neomutt.man
@@ -76,7 +76,7 @@
 .OP \-n
 .OP \-e command
 .OP \-F config
-.BR \-B
+.B \-B
 .YS
 .
 .SY neomutt
@@ -98,7 +98,7 @@
 .OP \-n
 .OP \-e command
 .OP \-F config
-.BI \-G
+.B \-G
 .YS
 .
 .SY neomutt
@@ -112,7 +112,7 @@
 .OP \-n
 .OP \-e command
 .OP \-F config
-.BI \-p
+.B \-p
 .YS
 .
 .SY neomutt
@@ -126,14 +126,14 @@
 .OP \-n
 .OP \-e command
 .OP \-F config
-.BI \-Z
+.B \-Z
 .YS
 .
 .SY neomutt
 .OP \-n
 .OP \-e command
 .OP \-F config
-.BI \-z
+.B \-z
 .OP \-f mailbox
 .YS
 .
@@ -144,7 +144,6 @@
 .\" --------------------------------------------------------------------
 .SH DESCRIPTION
 .\" --------------------------------------------------------------------
-.PP
 NeoMutt is a small but very powerful text based program for reading and sending
 electronic mail under Unix operating systems, including support for color
 terminals, MIME, OpenPGP, and a threaded sorting mode.
@@ -159,7 +158,7 @@
 .SH OPTIONS
 .\" --------------------------------------------------------------------
 .TP
-.BI \-\-
+.B \-\-
 Special argument forces NeoMutt to stop option parsing and treat remaining
 arguments as \fIaddress\fPes even if they start with a dash
 .
@@ -174,14 +173,13 @@
 .RS
 .IP
 .EX
-.BI "neomutt \-a " "image.jpg " "\-\- " "address1 "
-.BI "neomutt \-a " "image.jpg *.png " "\-\- " "address1 address2 "
+.BI "neomutt \-a " "image.jpg " "\-\- " "address1"
+.BI "neomutt \-a " "image.jpg *.png " "\-\- " "address1 address2"
 .EE
 .RE
-.IP
 .
 .TP
-.BI \-B
+.B \-B
 Run in batch mode (do not start the ncurses UI)
 .
 .TP
@@ -193,7 +191,7 @@
 Specify a carbon copy (Cc) recipient
 .
 .TP
-.BI \-D
+.B \-D
 Dump all configuration variables as
 .RB \(aq name = value \(aq
 pairs to stdout
@@ -217,7 +215,7 @@
 $debug_file)
 .
 .TP
-.BI \-E
+.B \-E
 Edit \fIdraft\fP (\fB\-H\fP) or \fIinclude\fP (\fB\-i\fP) file during message
 composition
 .
@@ -235,7 +233,7 @@
 Specify a \fImailbox\fP (as defined with \fBmailboxes\fP command) to load
 .
 .TP
-.BI \-G
+.B \-G
 Start NeoMutt with a listing of subscribed newsgroups
 .
 .TP
@@ -251,7 +249,7 @@
 it will be silently discarded.
 .
 .TP
-.BI \-h
+.B \-h
 Print this help message and exit
 .
 .TP
@@ -274,11 +272,11 @@
 The \fItype\fP is either MH, MMDF, Maildir or mbox (case-insensitive)
 .
 .TP
-.BI \-n
+.B \-n
 Do not read the system-wide configuration file
 .
 .TP
-.BI \-p
+.B \-p
 Resume a prior postponed message, if any
 .
 .TP
@@ -288,7 +286,7 @@
 Add -O for one-liner documentation.
 .
 .TP
-.BI \-R
+.B \-R
 Open mailbox in read-only mode
 .
 .TP
@@ -296,24 +294,24 @@
 Specify a \fIsubject\fP (must be enclosed in quotes if it has spaces)
 .
 .TP
-.BI \-v
+.B \-v
 Print the NeoMutt version and compile-time definitions and exit
 .
 .TP
-.BI \-vv
+.B \-vv
 Print the NeoMutt license and copyright information and exit
 .
 .TP
-.BI \-y
+.B \-y
 Start NeoMutt with a listing of all defined mailboxes
 .
 .TP
-.BI \-Z
+.B \-Z
 Open the first mailbox with new message or exit immediately with exit code 1 if
 none is found in all defined mailboxes
 .
 .TP
-.BI \-z
+.B \-z
 Open the first or specified (\fB\-f\fP) mailbox if it holds any message or exit
 immediately with exit code 1 otherwise
 .
@@ -439,7 +437,6 @@
 .\" --------------------------------------------------------------------
 .SS "\s-1Configuration files\s0"
 .\" --------------------------------------------------------------------
-.PP
 NeoMutt will read just the first found configuration file of system-wide and
 user-specific category, from the list below and in that order.
 .
@@ -504,26 +501,26 @@
 (from top to bottom) as they are specified in that listing.
 .
 .TP
-.IR "~/.mailcap"
+.I ~/.mailcap
 .TQ
-.IR "@MAN_SYSCONFDIR@/mailcap"
+.I "@MAN_SYSCONFDIR@/mailcap"
 User-specific and system-wide definitions for handling non-text MIME types,
 look at environment variable \fBMAILCAPS\fP above for additional search
 locations.
 .
 .TP
-.IR "~/.neomuttdebug0"
+.I ~/.neomuttdebug0
 User's default debug log file. For further details or customising file path see
 command line options \fB\-d\fP and \fB\-l\fP above.
 .
 .TP
-.IR "/etc/mime.types"
+.I /etc/mime.types
 .TQ
-.IR "@MAN_SYSCONFDIR@/mime.types"
+.I "@MAN_SYSCONFDIR@/mime.types"
 .TQ
-.IR "@MAN_DATADIR@/mime.types"
+.I "@MAN_DATADIR@/mime.types"
 .TQ
-.IR "~/.mime.types"
+.I ~/.mime.types
 Description files for simple plain text mapping between MIME types and filename
 extensions. NeoMutt parses these files in the stated order while processing
 attachments to determine their MIME type.
@@ -533,22 +530,20 @@
 The full NeoMutt manual in HTML, PDF or plain text format.
 .
 .TP
-.IR "/tmp/neomutt-XXXX-XXXX-XXXX"
+.I /tmp/neomutt-XXXX-XXXX-XXXX
 Temporary files created by NeoMutt. For custom locations look at description of
 the environment variable \fBTMPDIR\fP above. Notice that the suffix
-\fI-XXXX-XXXX-XXXX\fP is just a placeholder for, e.g. hostname, user name/ID,
+\fI-XXXX-XXXX-XXXX\fP is just a placeholder for, e.g., hostname, user name/ID,
 process ID and/or other random data.
 .
 .\" --------------------------------------------------------------------
 .SH BUGS
 .\" --------------------------------------------------------------------
-.PP
 See issue tracker at <https://github.com/neomutt/neomutt/issues>.
 .
 .\" --------------------------------------------------------------------
 .SH NO WARRANTIES
 .\" --------------------------------------------------------------------
-.PP
 This program is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 A PARTICULAR PURPOSE. See the GNU General Public License for more details.
@@ -556,7 +551,6 @@
 .\" --------------------------------------------------------------------
 .SH SEE ALSO
 .\" --------------------------------------------------------------------
-.PP
 .\" sorted by category and name
 .BR gettext (1),
 .BR msmtp (1),
@@ -583,7 +577,6 @@
 .\" --------------------------------------------------------------------
 .SH AUTHOR
 .\" --------------------------------------------------------------------
-.PP
 Michael Elkins, and others. Use <neomutt-devel@\:neomutt.org> to contact the
 developers.
 .
--- a/docs/config.c
+++ b/docs/config.c
@@ -4148,7 +4148,7 @@
 ** .dt %D .dd     .dd Descriptive name of the mailbox
 ** .dt %F .dd *   .dd Number of flagged messages in the mailbox
 ** .dt %L .dd * @ .dd Number of messages after limiting
-** .dt %n .dd     .dd 'N' if mailbox has new mail, ' ' (space) otherwise
+** .dt %n .dd     .dd "N" if mailbox has new mail, ' ' (space) otherwise
 ** .dt %N .dd *   .dd Number of unread messages in the mailbox (seen or unseen)
 ** .dt %o .dd *   .dd Number of old messages in the mailbox (unread, seen)
 ** .dt %r .dd *   .dd Number of read messages in the mailbox (read, seen)
